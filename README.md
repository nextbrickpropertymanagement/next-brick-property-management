Next Brick, all we do is property management. We focus on delivering a trusted, modern rental experience for our property owners & tenants. Great Service, full transparency, and responsive communications.

Website : http://nextbrick.co